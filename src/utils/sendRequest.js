const base_url = process.env.VUE_APP_BASE_URL;

const sendRequest = async (route, request, isFile = false) => {
  let response;
  try {
    response = await fetch(base_url + route, request);
    if (response.status === 401 || response.status == 500 || response.status == 403) {
      //notificationponseAndRedirect('error', 'Sesión actual', 'Sesión expirada');
      return { success: false };
    }
    if (response.status === 420) {
      return { success: false };
    }
  } catch (error) {
    return { success: false };
  }
  try {
    const data = isFile && response.status === 200 ? await response.blob() : await response.json();
    return data;
  } catch (error) {
    return { success: false };
  }
};

export default { sendRequest };
