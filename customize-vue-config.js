module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? process.env.VUE_APP_SUB_ROUTE
        ? process.env.VUE_APP_SUB_ROUTE
        : process.env.BASE_URL
      : process.env.BASE_URL,
  // devServer: {
  //   port: 8080,
  //   proxy: 'http://10.9.204.21/backend/services/',
  // },
};
