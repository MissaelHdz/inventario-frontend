import "bootstrap/dist/css/bootstrap.css"
import { createRouter, createWebHashHistory } from 'vue-router'
import { createApp } from 'vue'
import App from './App.vue'

import 'bootstrap/dist/js/bootstrap'

const routes = [
    {
        path: '/', component: () => import('./components/DataTable.vue')
    },
    {
        path: '/table', component: () => import('./components/DataTable.vue')
    },
    {
        path: '/new-product', component: () => import('./components/NewProduct.vue')
    },
    {
        path: '/table/:id', component: () => import('./components/EditProduct.vue')
    },
    {
        path: '/table/view/:id', component: () => import('./components/DetailProduct.vue')
    },
]

const router = createRouter({
    routes,
    history: createWebHashHistory(),
})

const app = createApp(App)
app.use(router)
app.mount('#app')


