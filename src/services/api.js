import axios from 'axios'

const base_url = process.env.VUE_APP_BASE_URL;

const getTable = () => {

  return axios.get(base_url + 'product')
  .then((response) => { 
    return response.data
  })
};

const getTablePages = (page) => {

  return axios.get(base_url + `product?page=${page}`)
  .then((response) => { 
    return response.data
  })
};

const getProduct = (id) => {

  return axios.get(base_url + `product/${id}`)
  .then((response) => { 
    return response.data
  })
};

const rateProduct = (id, rate) => {

  return axios.post(base_url + `rate/${id}`, { 'score' : rate })
  .then((response) => { 
    return response.data
  })
};

const getCategories = () => {

  return axios.get(base_url + `product/create`)
  .then((response) => { 
    return response.data
  })
};

const createProduct = (body) => {

  return axios.post(base_url + `product`, body)
  .then((response) => { 
    return response.data
  })
};

const updateProduct = (id, body) => {

  return axios.put(base_url + `product/${id}`, body)
  .then((response) => { 
    return response.data
  })
};

const updateStock = (id, status) => {

  return axios.put(base_url + `inventory/${id}`, { 'status': status})
  .then((response) => { 
    return response.data
  })
};

const deleteProduct = (id) => {

  return axios.delete(base_url + `product/${id}`)
  .then((response) => { 
    return response.data
  })
};

export { getTable, getTablePages, getCategories, createProduct, getProduct, rateProduct, updateProduct, updateStock, deleteProduct };